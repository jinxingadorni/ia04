# IA04 Projet système de vote

Ce projet consiste à construire un package go qui nous permet de créer un serveur de vote, ainsi que des agents de vote.
Le serveur permet de créer un scrutin avec une fonction de choix social au choix, d'ajouter un vote à un scrutin, de lister les scrutins et d'obtenir les résultats d'un scrutin.

Réalisé par : Pierre ADORNI et Jinxing LAI

## Pour charger le package par gitlab

En premier lieu, il faut autoriser l'installation de modules depuis le gitlab de l'utc si ce n'est pas déjà fait
```bash
  set GOPRIVATE=gitlab.utc.fr
```
```bash
  go get gitlab.utc.fr/jinxingadorni/ia04.git@v0.1.0
```
Installer les commandes
```bash
  go install gitlab.utc.fr/jinxingadorni/ia04.git/cmd/list-ballots@latest
  go install gitlab.utc.fr/jinxingadorni/ia04.git/cmd/new-ballot@latest
  go install gitlab.utc.fr/jinxingadorni/ia04.git/cmd/result-ballot@latest
  go install gitlab.utc.fr/jinxingadorni/ia04.git/cmd/voting-server@latest
```


## Documentation

1. Commande `voting-server`

Cette commande nous permet de lancer à l'adresse http://localhost:8080 un serveur local qui gère le système du vote.

Il y a quatre actions possibles sur le serveur:

 - POST http://localhost:8080/new-ballot: Crée un scrutin avec la fonction de choix social spécifiée, les IDs des votants, une deadline, un nombre d'alternatives(canditatures) et une fonction de tie-break.

 - GET http://localhost:8080/ballots: Liste les scrutins existants.

 - POST http://localhost:8080/vote: Ajoute un vote à un scrutin. Prends un id de votant, id de scrutin, des préférences et une éventuelle option (seuil pour le vote par approbation).

 - POST http://localhost:8080/result: Renvoie les résultats d'un scrutin. Prends en paramètre l'identifiant du scrutin.

Vous pouvez directement envoyer des requêtes à ce serveur par l'extension *REST Plugin*.

2. Commande `new-ballot`

Cette commande nous permet de créer un nouveau scrutin sur notre serveur. Il faut correctement saisir les infos par clavier

3. Commande `list-ballots`

Cette commande nous permet de lister tous les scrutins sur notre serveur.

4. Commande `result-ballot`

Cette commande nous permet d'obtenir le résultats du scrutin par un identifiant entré au clavier.

Nous avons également fait en sorte de gérer certaines erreurs potentielles: on ne peut pas de prendre le résultats avant que le vote soit terminé, on ne peut pas créer un scrutin qui termine dans le passé, on ne peut que voter une seule fois par un votant, etc.


## Ballot Struct

| Field Name | Data Type                      | Description                               |
|------------|--------------------------------|-------------------------------------------|
| Id         | `string`                       | The identifier for the ballot.            |
| profile    | `comsoc.Profile`               | The profile for the comsoc system.        |
| options    | `[][]int`                      | The options available in the ballot.      |
| scf        | `interface{}`                  | The social choice function.               |
| deadline   | `time.Time`                    | The deadline for voting.                  |
| voterIds   | `[]string`                     | The list of voter IDs.                    |
| altsCount  | `int`                          | The count of alternatives.                |
| votedMap   | `map[string]bool`              | A map tracking who has voted.             |

## BallotCreationRequest Struct

| Field Name | Data Type                      | Description                                  |
|------------|--------------------------------|----------------------------------------------|
| Rule       | `string`                       | The rule for the ballot creation.            |
| Deadline   | `time.Time`                    | The deadline for the ballot.                 |
| VoterIds   | `[]string`                     | The list of IDs of voters.                   |
| AltsCount  | `int`                          | The count of alternatives, marked as `#alts`.|
| TieBreak   | `[]comsoc.Alternative`         | The tie-breaking alternatives.               |

## BallotCreationResponse Struct

| Field Name | Data Type                      | Description                       |
|------------|--------------------------------|-----------------------------------|
| BallotId   | `string`                       | The identifier of the created ballot. |

## VoteRequest Struct

| Field Name | Data Type                      | Description                                  |
|------------|--------------------------------|----------------------------------------------|
| BallotId   | `string`                       | The identifier for the ballot.               |
| AgentId    | `string`                       | The identifier of the voting agent.          |
| Prefs      | `[]comsoc.Alternative`         | The preferences of the voter.                |
| Options    | `[]int`                        | The options associated with the vote.        |

## ResultRequest Struct

| Field Name | Data Type                      | Description                          |
|------------|--------------------------------|--------------------------------------|
| BallotId   | `string`                       | The identifier for the ballot.       |

## ResultResponse Struct

| Field Name | Data Type                      | Description                  |
|------------|--------------------------------|------------------------------|
| Winner     | `comsoc.Alternative`           | The winning alternative.     |

## VotingServer Struct

| Field Name | Data Type                      | Description                            |
|------------|--------------------------------|----------------------------------------|
| Addr       | `string`                       | The address of the voting server.      |
| Ballots    | `[]*Ballot`                    | A slice of pointers to Ballot structs. |
