package clients

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
	"gitlab.utc.fr/jinxingadorni/ia04.git/srv"
)

type ClientVotant struct {
	id        string
	url       string
	agent_id  string
	ballot_id string
	prefs     []comsoc.Alternative
	options   []int
}

func NewClientVotant(id string, url string, agent_id string, ballot_id string, prefs []comsoc.Alternative, options []int) *ClientVotant {
	return &ClientVotant{id, url, agent_id, ballot_id, prefs, options}
}

func (agentVotant *ClientVotant) treatVoteResponse(resp *http.Response) (res string) {
	var ballotCreationResponse srv.BallotCreationResponse
	json.NewDecoder(resp.Body).Decode(&ballotCreationResponse)
	res = ballotCreationResponse.BallotId
	return
}

func (agentVotant *ClientVotant) doVoteRequest() (string, error) {
	// Serialize the request
	url := agentVotant.url + "/vote"
	data, err := json.Marshal(srv.VoteRequest{
		AgentId:  agentVotant.agent_id,
		BallotId: agentVotant.ballot_id,
		Prefs:    agentVotant.prefs,
		Options:  agentVotant.options,
	})
	if err != nil {
		return "", fmt.Errorf("error marshaling request data: %w", err)
	}

	// Send the request
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return "", fmt.Errorf("error sending request to %s: %w", url, err)
	}
	defer resp.Body.Close()

	// Handle the response
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("[%d] unexpected response status: %s", resp.StatusCode, resp.Status)
	}

	return agentVotant.treatVoteResponse(resp), nil
}

type tmpBallot struct {
	Id string `json:"Id"`
}

type BallotListResponse []tmpBallot

func (agentVotant *ClientVotant) doListRequest() ([]tmpBallot, error) {
	url := agentVotant.url + "/ballots"

	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("error sending request to %s: %w", url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("[%d] unexpected response status: %s", resp.StatusCode, resp.Status)
	}

	var ballots BallotListResponse
	err = json.NewDecoder(resp.Body).Decode(&ballots)
	if err != nil {
		return nil, fmt.Errorf("error decoding response: %w", err)
	}

	return ballots, nil
}

func (agentVotant *ClientVotant) ListBallots() error {
	ballots, err := agentVotant.doListRequest()
	if err != nil {
		return err
	}

	for _, ballot := range ballots {
		fmt.Printf("Ballot ID: %s\n", ballot.Id)
	}

	return nil
}

func (agentVotant *ClientVotant) treatResultVoteResponse(resp *http.Response) (res string, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	var resultResponse srv.ResultResponse
	err = json.Unmarshal(buf.Bytes(), &resultResponse)
	res = string(resultResponse.Winner)
	if err != nil {
		return "", err
	}

	return
}

func (agentVotant *ClientVotant) doResultRequest() (string, error) {
	req := srv.ResultRequest{
		BallotId: agentVotant.ballot_id,
	}
	url := agentVotant.url + "/result"
	// HTTP POST: Envoie l'ID du ballot a "/result" et recupere la reponse dans la variable "response"
	jsonData, _ := json.Marshal(req)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return "", fmt.Errorf("error sending request to %s: %w", url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("[%d] unexpected response status: %s", resp.StatusCode, resp.Status)
	}

	// Traitement de la reponse du serveur
	winner, err := agentVotant.treatResultVoteResponse(resp)
	if err != nil {
		return "", fmt.Errorf("error decoding response: %w", err)
	}

	return winner, nil
}

func (agentVotant *ClientVotant) ResultBallot() error {
	winner, err := agentVotant.doResultRequest()
	if err != nil {
		return err
	}

	fmt.Printf("Winner: %s\n", winner)

	return nil
}

func (votantAgent *ClientVotant) Start() {
	log.Printf("Démarrage de Client Votant: %s", votantAgent.id)
	ballot_id, err := votantAgent.doVoteRequest()
	log.Printf("Client Votant: %s voted for %s", votantAgent.id, ballot_id)

	if err != nil {
		log.Fatal(votantAgent.id, "error:", err.Error())
	} else {
		log.Printf("%s", ballot_id)
	}
}
