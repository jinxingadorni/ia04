package main

import (
	"fmt"
	"gitlab.utc.fr/jinxingadorni/ia04.git/agt"
	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
	"math/rand"
	"time"
)

func main() {
	serverChannel := make(chan comsoc.VotingMessage)
	server := comsoc.VotingServer{
		C: serverChannel,
		Scf: comsoc.SCFFactory(
			comsoc.BordaSCF,
			comsoc.TieBreakFactory([]comsoc.Alternative{1, 2, 3, 4}),
		),
	}
	go server.StartServer()

	for i := 0; i < 20; i++ {
		prefs := []comsoc.Alternative{1, 2, 3, 4}
		rand.Shuffle(len(prefs), func(i, j int) {
			prefs[i], prefs[j] = prefs[j], prefs[i]
		})
		agent := agt.Agent{
			ID:         uint(i),
			Name:       fmt.Sprintf("Agent %d", i),
			Prefs:      prefs,
			ServerChan: serverChannel,
		}
		go agent.Start()
	}

	server.StartVote(10 * time.Second)

	time.Sleep(20 * time.Second)

	server.StopServer()
}
