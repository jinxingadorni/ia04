module gitlab.utc.fr/jinxingadorni/ia04.git

go 1.20

require (
	github.com/google/uuid v1.4.0 // indirect
	gitlab.utc.fr/lagruesy/ia04 v0.2.1 // indirect
)
