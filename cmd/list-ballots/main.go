package main

import (
	"gitlab.utc.fr/jinxingadorni/ia04.git/agt/clients"
	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
)

func main() {
	client := clients.NewClientVotant("forList", "http://localhost:8080", "0", "0", []comsoc.Alternative{0, 1, 2, 3}, []int{})
	client.ListBallots()
}
