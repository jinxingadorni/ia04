package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
	"gitlab.utc.fr/jinxingadorni/ia04.git/srv"
)

func main() {
	fmt.Println("Please as the order enter the following information: the rule, deadline, voter-ids, #alts and tie-break of the ballot you want to create:")
	var rule string
	var deadline time.Time
	var voterIds []string
	var altsCount int
	var tieBreak []comsoc.Alternative
	fmt.Scanln(&rule)
	fmt.Scanln(&deadline)
	fmt.Scanln(&voterIds)
	fmt.Scanln(&altsCount)
	fmt.Scanln(&tieBreak)
	// Serialize the request
	url := "http://localhost:8080" + "/new-ballot"
	data, err := json.Marshal(srv.BallotCreationRequest{
		Rule:      rule,
		Deadline:  deadline,
		VoterIds:  voterIds,
		AltsCount: altsCount,
		TieBreak:  tieBreak,
	})
	if err != nil {
		fmt.Errorf("error marshaling request data: %w", err)
	}

	// Send the request
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		fmt.Errorf("error sending request to %s: %w", url, err)
	}
	defer resp.Body.Close()

	// Handle the response
	if resp.StatusCode != http.StatusOK {
		fmt.Errorf("[%d] unexpected response status: %s", resp.StatusCode, resp.Status)
	}

	var ballotCreationResponse srv.BallotCreationResponse
	json.NewDecoder(resp.Body).Decode(&ballotCreationResponse)
	fmt.Sprint("New ballot created with id: %s", ballotCreationResponse.BallotId)
}
