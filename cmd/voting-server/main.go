package main

import "gitlab.utc.fr/jinxingadorni/ia04.git/srv"

func main() {
	b := srv.Ballot{}
	server := srv.VotingServer{
		Addr:    ":8080",
		Ballots: []*srv.Ballot{&b},
	}
	server.Start()
}
