package main

// Path: cmd/result-ballot/main.go

import (
	"fmt"

	"gitlab.utc.fr/jinxingadorni/ia04.git/agt/clients"
	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
)

func main() {
	fmt.Println("Enter the id of the ballot you want to see the result of:")
	var id string
	fmt.Scanln(&id)
	client := clients.NewClientVotant("forresult", "http://localhost:8080", "0", id, []comsoc.Alternative{0, 1, 2, 3}, []int{})
	client.ResultBallot()
}
