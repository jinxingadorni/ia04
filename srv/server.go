package srv

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
)

type Ballot struct {
	sync.Mutex
	Id        string
	profile   comsoc.Profile
	options   [][]int
	scf       interface{}
	deadline  time.Time
	voterIds  []string
	altsCount int
	votedMap  map[string]bool
}

func (b *Ballot) AddVote(prefs []comsoc.Alternative, options []int) error {
	// TODO: validate Alternatives (check the voter does not vote for more than altsCount alternatives)
	if len(prefs) > b.altsCount {
		return errors.New("too many alternatives in the vote")
	}
	var maxAlt, minAlt comsoc.Alternative
	for _, alt := range prefs {
		if alt < minAlt {
			minAlt = alt
		}
		if alt > maxAlt {
			maxAlt = alt
		}
	}

	if minAlt < 0 || maxAlt > comsoc.Alternative(b.altsCount) {
		return errors.New("invalid alternative in the vote")
	}

	b.Lock()
	defer b.Unlock()

	b.profile = append(b.profile, prefs)
	b.options = append(b.options, options)
	return nil
}

type BallotCreationRequest struct {
	Rule      string               `json:"rule"`
	Deadline  time.Time            `json:"deadline"`
	VoterIds  []string             `json:"voter-ids"`
	AltsCount int                  `json:"#alts"`
	TieBreak  []comsoc.Alternative `json:"tie-break"`
}

type BallotCreationResponse struct {
	BallotId string `json:"ballot-id"`
}

type VoteRequest struct {
	BallotId string               `json:"ballot-id"`
	AgentId  string               `json:"agent-id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Options  []int                `json:"options"`
}

type ResultRequest struct {
	BallotId string `json:"ballot-id"`
}

type ResultResponse struct {
	Winner comsoc.Alternative `json:"winner"`
}

type VotingServer struct {
	Addr    string
	Ballots []*Ballot
}

func (s *VotingServer) handleBallotCreation(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var req BallotCreationRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("error decoding request: %s", err.Error())))
		return
	}

	// validate the existence of required fields
	if req.Rule == "" || req.Deadline.IsZero() || len(req.VoterIds) == 0 || req.AltsCount == 0 || len(req.TieBreak) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing required fields"))
		return
	}

	// validate rule
	var scf interface{}
	switch req.Rule {
	case "majority":
		scf = comsoc.MajoritySCF
	case "borda":
		scf = comsoc.BordaSCF
	case "approval":
		scf = comsoc.ApprovalSCF
	case "stv":
		scf = comsoc.STV_SCF
	case "copeland":
		scf = comsoc.CopelandSCF
	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte(fmt.Sprintf("unknown rule %q", req.Rule)))
		return
	}

	// TODO: validate tiebreak
	if len(req.TieBreak) > req.AltsCount {
		//return errors.New("too many alternatives in the vote")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("too many alternatives in the vote"))
		return
	}
	var maxAlt, minAlt comsoc.Alternative
	for _, alt := range req.TieBreak {
		if alt < minAlt {
			minAlt = alt
		}
		if alt > maxAlt {
			maxAlt = alt
		}
	}

	if minAlt < 0 || maxAlt > comsoc.Alternative(req.AltsCount) {
		//return errors.New("invalid alternative in the vote")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("invalid alternative in the vote"))
		return
	}

	//validate deadline
	if time.Now().After(req.Deadline) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("deadline is in the past"))
		return
	}

	tiebreak := comsoc.TieBreakFactory(interface{}(req.TieBreak).([]comsoc.Alternative))
	tiebreakedScf := comsoc.SCFFactory(scf.(func(p comsoc.Profile) ([]comsoc.Alternative, error)), tiebreak)

	ballot := Ballot{
		Id:        uuid.New().String(),
		scf:       tiebreakedScf,
		deadline:  req.Deadline,
		voterIds:  req.VoterIds,
		altsCount: req.AltsCount,
		votedMap:  make(map[string]bool),
	}

	s.Ballots = append(s.Ballots, &ballot)
	log.Printf("Created ballot %q, enabling %d voters to choose between %d alternatives using the %q rule, deadline is %s", ballot.Id, len(ballot.voterIds), ballot.altsCount, req.Rule, ballot.deadline.Format(time.RFC3339))

	w.WriteHeader(http.StatusCreated)
	resp := BallotCreationResponse{
		BallotId: ballot.Id,
	}
	serial, _ := json.Marshal(resp)
	_, err = w.Write(serial)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s *VotingServer) handleBallotList(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(s.Ballots)
	_, err := w.Write(serial)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s *VotingServer) handleVote(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var req VoteRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("error decoding request: %s", err.Error())))
		return
	}

	// validate the existence of required fields
	if req.BallotId == "" || req.AgentId == "" || len(req.Prefs) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("missing required fields"))
		return
	}

	var ballot *Ballot
	for _, b := range s.Ballots {
		if b.Id == req.BallotId {
			ballot = b
			break
		}
	}

	if ballot.Id == "" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("unknown ballot %q", req.BallotId)))
		return
	}

	// TODO: validate AgentId
	flag := false
	for _, v := range ballot.voterIds {
		if v == req.AgentId {
			flag = true
		}
	}
	if !flag {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("unknown agent %q", req.AgentId)))
		return
	}
	// TODO: check agent has not already voted
	if ballot.votedMap[req.AgentId] {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("agent %q has already voted", req.AgentId)))
		return
	}
	// TODO: check deadline is not passed
	if time.Now().After(ballot.deadline) {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("Voting deadline has passed")))
		return
	}

	fmt.Printf("ballot before: %v\n", ballot.profile)
	err = ballot.AddVote(req.Prefs, req.Options)
	fmt.Printf("ballot after: %v\n", ballot.profile)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("error adding vote: %s", err.Error())))
		return
	}

	log.Printf("%q voted %v (opts=%v) on ballot %q\n", req.AgentId, req.Prefs, req.Options, req.BallotId)
	ballot.votedMap[req.AgentId] = true
	w.WriteHeader(http.StatusOK)
}

func (s *VotingServer) handleResultRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	var req ResultRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("error decoding request: %s", err.Error())))
		return
	}

	var ballot *Ballot
	for _, b := range s.Ballots {
		if b.Id == req.BallotId {
			ballot = b
			break
		}
	}

	if ballot.Id == "" {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(fmt.Sprintf("unknown ballot %q", req.BallotId)))
		return
	}

	fmt.Printf("%v\n", ballot)

	// TODO: check that either the deadline is passed, or all voters have voted
	flagAllVoted := true
	for _, v := range ballot.voterIds {
		if !ballot.votedMap[v] {
			flagAllVoted = false
		}
	}
	if !time.Now().After(ballot.deadline) || !flagAllVoted {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("Voting deadline has not passed")))
		return
	}

	var winner comsoc.Alternative
	// use reflect to check if the SCF takes in a Profile or a Profile and options
	numInput := reflect.TypeOf(ballot.scf).NumIn()
	if numInput == 1 {
		scf := ballot.scf.(func(comsoc.Profile) (comsoc.Alternative, error))
		winner, err = scf(ballot.profile)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("error calculating result: %s", err.Error())))
			return
		}
	} else if numInput == 2 {
		scf := ballot.scf.(func(comsoc.Profile, [][]int) (comsoc.Alternative, error))
		winner, err = scf(ballot.profile, ballot.options)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(fmt.Sprintf("error calculating result: %s", err.Error())))
			return
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("invalid SCF")))
		return
	}

	log.Printf("Winner of ballot %q is %d\n", req.BallotId, winner)
	w.WriteHeader(http.StatusOK)
	resp := ResultResponse{
		Winner: winner,
	}
	serial, _ := json.Marshal(resp)
	_, err = w.Write(serial)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s *VotingServer) Start() {
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", s.handleBallotCreation)
	mux.HandleFunc("/ballots", s.handleBallotList)
	mux.HandleFunc("/vote", s.handleVote)
	mux.HandleFunc("/result", s.handleResultRequest)

	srv := http.Server{
		Addr:    s.Addr,
		Handler: mux,
	}

	log.Println("Listening on", s.Addr)
	go log.Fatal(srv.ListenAndServe())
}
