package srv

import (
	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
	"testing"
)

func TestBallot_AddVote(t *testing.T) {
	b := Ballot{
		altsCount: 4,
	}
	b.AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	b.AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	b.AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	b.AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	if len(b.profile) != 4 {
		t.Errorf("Expected 4 votes, got %d", len(b.profile))
	}

	s := VotingServer{}
	b1 := Ballot{
		altsCount: 4,
	}
	s.Ballots = append(s.Ballots, &b1)

	s.Ballots[0].AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	s.Ballots[0].AddVote([]comsoc.Alternative{0, 1, 2, 3}, []int{})
	s.Ballots[0].AddVote([]comsoc.Alternative{2, 1, 0, 3}, []int{})
	s.Ballots[0].AddVote([]comsoc.Alternative{1, 2, 0, 3}, []int{})

	if len(s.Ballots[0].profile) != 4 {
		t.Errorf("Expected 4 votes, got %d", len(s.Ballots[0].profile))
	}

}
