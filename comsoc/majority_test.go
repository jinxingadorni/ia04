package comsoc

import (
	"reflect"
	"testing"
)

func TestMajoritySWF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{1, 2, 3, 4},
		{3, 2, 1, 4},
		{2, 3, 1, 4},
	}
	count, err := MajoritySWF(p)
	if err != nil {
		t.Errorf(err.Error())
	}

	expectedCount := Count{
		1: 2,
		2: 1,
		3: 1,
		4: 0,
	}

	if !reflect.DeepEqual(count, expectedCount) {
		t.Errorf("%+v was computed, expected %+v", count, expectedCount)
	}
}

func TestMajoritySCF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{1, 2, 3, 4},
		{3, 2, 1, 4},
		{2, 3, 1, 4},
	}
	alts, err := MajoritySCF(p)
	if err != nil {
		t.Errorf(err.Error())
	}

	if len(alts) != 1 || alts[0] != 1 {
		t.Errorf("Expected [1], got %+v", alts)
	}
}
