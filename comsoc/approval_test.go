package comsoc

import (
	"reflect"
	"testing"
)

func TestApprovalSWF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{1, 2, 3, 4},
		{3, 2, 1, 4},
		{2, 3, 1, 4},
	}
	thresholds := []int{1, 2, 3, 4}
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		t.Errorf(err.Error())
	}

	expectedCount := Count{
		1: 4,
		2: 3,
		3: 2,
		4: 1,
	}

	if !reflect.DeepEqual(count, expectedCount) {
		t.Errorf("%+v was computed, expected %+v", count, expectedCount)
	}
}

func TestApprovalSCF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{1, 2, 3, 4},
		{3, 2, 1, 4},
		{2, 3, 1, 4},
	}
	thresholds := []int{1, 2, 3, 4}
	alts, err := ApprovalSCF(p, thresholds)
	if err != nil {
		t.Errorf(err.Error())
	}

	if len(alts) != 1 || alts[0] != 1 {
		t.Errorf("Expected [1], got %+v", alts)
	}
}
