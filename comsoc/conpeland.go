package comsoc

/*
Règle de Copeland (Copeland’s rule):

Le meilleur candidat est celui qui bat le plus d’autres candidats
On associe à chaque candidat aaa le score suivant : pour chaque autre candidat b≠a, +1 si une majorité préfère a à b, −1 si une majorité préfère bbb à aaa et 000 sinon
Le candidat élu est celui qui a le plus haut score de Copeland
*/
func CopelandSWF(p Profile) (Count, error) {
	unique := UniqueAlternatives(p)
	// check that all alternatives are in the profile
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}

	// count the number of times each alternative wins
	count := make(Count)
	for _, alt1 := range unique {
		for _, alt2 := range unique {
			if Condorcet1v1(p, alt1, alt2) == 1 {
				count[alt1]++
			}
		}
	}

	return count, nil
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), nil
}
