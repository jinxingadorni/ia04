package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	unique := UniqueAlternatives(p)
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}

	votes := make(Count, len(unique))
	for _, alt := range unique {
		votes[alt] = 0
	}
	for _, order := range p {
		votes[order[0]] += 1
	}

	return votes, nil
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}

	return maxCount(count), nil
}
