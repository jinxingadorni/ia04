package comsoc

import (
	"errors"
	"fmt"
)

// ApprovalSWF implements approval voting, thresholds are the index from which alternatives are not approved (included)
func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	unique := UniqueAlternatives(p)
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}
	if len(p) != len(thresholds) {
		return nil, errors.New(
			fmt.Sprintf("profile has %d voters but %d thresholds were provided", len(p), len(thresholds)),
		)
	}

	approvals := make(Count)
	for _, alt := range unique {
		approvals[alt] = 0
	}

	for i, order := range p {
		for j, alt := range order {
			if j < thresholds[i] {
				approvals[alt] += 1
			}
		}
	}

	return approvals, nil
}

// ApprovalSCF implements approval voting, thresholds are the index from which alternatives are not approved (included)
func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}

	return maxCount(count), nil
}
