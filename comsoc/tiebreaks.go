package comsoc

import (
	"errors"
	"fmt"
	"math"
	"sort"
)

func TieBreakFactory(orderedAlts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alts []Alternative) (Alternative, error) {
		if len(alts) == 0 {
			return -1, errors.New("empty input slice")
		}

		minRank := int(math.Inf(1))
		var minAlt Alternative = -1
		for _, alt := range alts {
			if rank := Rank(alt, orderedAlts); rank != -1 {
				if rank < minRank {
					minRank = rank
					minAlt = alt
				}
			} else {
				return -1, errors.New(fmt.Sprintf("alternative %d was not specified in tiebreak order", alt))
			}
		}

		return minAlt, nil
	}
}

func SWFFactory(swf func(p Profile) (Count, error), tieBreak func([]Alternative) (Alternative, error)) func(p Profile) (alt []Alternative, err error) {
	return func(p Profile) ([]Alternative, error) {
		count, err := swf(p)
		if err != nil {
			return nil, err
		}
		order := make([]Alternative, 0, len(count))
		for alt := range count {
			order = append(order, alt)
		}
		sort.Slice(order, func(i, j int) bool {
			if count[order[i]] == count[order[j]] {
				winner, err := tieBreak([]Alternative{order[i], order[j]})
				if err != nil {
					err.Error()
					return false
				}
				return winner == order[j]
			}
			return count[order[i]] > count[order[j]]
		})
		return order, nil
	}
}

func SCFFactory(scf func(p Profile) ([]Alternative, error), tieBreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (Alternative, error) {
		// implementation here
		bestAlts, err := scf(p)
		if err != nil {
			return -1, err
		}
		if len(bestAlts) == 1 {
			return bestAlts[0], nil
		}
		return tieBreak(bestAlts)
	}
}
