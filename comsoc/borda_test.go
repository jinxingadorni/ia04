package comsoc

import (
	"reflect"
	"sort"
	"testing"
)

func TestBordaSWF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{4, 1, 2, 3},
		{3, 4, 1, 2},
		{2, 3, 4, 1},
	}
	count, err := BordaSWF(p)
	if err != nil {
		t.Errorf(err.Error())
	}
	expectedCount := Count{
		1: 6,
		2: 6,
		3: 6,
		4: 6,
	}
	if !reflect.DeepEqual(count, expectedCount) {
		t.Errorf("%+v was computed, expected %+v", count, expectedCount)
	}
}

func TestBordaSCF(t *testing.T) {
	p := Profile{
		{1, 2, 3, 4},
		{4, 1, 2, 3},
		{3, 4, 1, 2},
		{2, 3, 4, 1},
	}
	bestAlts, err := BordaSCF(p)
	if err != nil {
		t.Errorf(err.Error())
	}
	expectedAlts := []Alternative{1, 2, 3, 4}

	sort.Slice(bestAlts, func(i int, j int) bool { return bestAlts[i] < bestAlts[j] })
	// checkProfile is an alternative
	if !reflect.DeepEqual(bestAlts, expectedAlts) {
		t.Errorf("%+v was computed, expected %+v", bestAlts, expectedAlts)
	}

}
