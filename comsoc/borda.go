package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	unique := UniqueAlternatives(p)
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}
	//	nbVotant := len(p)
	tab := make(map[Alternative]int)
	for votant := range p {
		for idx, alt := range p[votant] {
			tab[alt] += len(p[votant]) - idx - 1
		}
	}
	return tab, nil
}
func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), nil
}
