package comsoc

/*
Vote Simple Transférable (Single Transferable Vote (STV):

Chaque individu indique donne son ordre de préférence $>_i$
Pour n candidats, on fait n−1 tours (à moins d’avoir avant une majorité stricte pour un candidat)
On suppose qu’à chaque tour chaque individu “vote” pour son candidat préféré (parmi ceux encore en course)
À chaque tour on élimine le plus mauvais candidat (celui qui a le moins de voix)
*/

func STV_SWF(p Profile) (Count, error) {
	unique := UniqueAlternatives(p)
	// check that all alternatives are in the profile
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}

	// count the number of times each alternative wins
	count := make(Count)
	for _, alt1 := range unique {
		count[alt1] = 0
	}
	resCount := make(Count)
	for i := 0; i < len(unique)-1; i++ {
		altElinimated, err := EliminatedSTV(p, count)
		if err != nil {
			return nil, err
		}
		for _, alt := range altElinimated {
			resCount[alt] = -(len(unique) - i)
		}
	}

	return resCount, nil

}
func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := STV_SWF(p)
	if err != nil {
		return nil, err
	}
	return maxCount(count), nil
}
