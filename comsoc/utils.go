// types de base et fonction utilitaires.

package comsoc

import (
	"fmt"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

// renvoie l'indice ou se trouve alt dans prefs, ou -1 si alt n'est pas dans prefs
func Rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if v == alt {
			return i
		}
	}
	return -1
}

// renvoie vrai ssi alt1 est préférée à alt2
func IsPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return Rank(alt1, prefs) < Rank(alt2, prefs)
}

// renvoie les meilleures alternatives pour un décomtpe donné
func maxCount(count Count) (bestAlts []Alternative) {
	var max int
	for alt, v := range count {
		if v > max {
			bestAlts = []Alternative{alt}
			max = v
		} else if v == max {
			bestAlts = append(bestAlts, alt)
		}
	}
	return bestAlts
}

// renvoie les mauvaies alternatives pour un décomtpe donné
func minCount(count Count) (bestAlts []Alternative) {
	var min int
	for alt, v := range count {
		if v < min {
			bestAlts = []Alternative{alt}
			min = v
		} else if v == min {
			bestAlts = append(bestAlts, alt)
		}
	}
	return bestAlts
}

// vérifie les préférences d'un agent, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois
func checkProfile(prefs []Alternative, alts []Alternative) error {
	if len(prefs) != len(alts) {
		return fmt.Errorf("preferences length %d do not match with alternatives %d", len(prefs), len(alts))
	}
	m := make(map[Alternative]bool)
	for _, alt := range prefs {
		if m[alt] {
			return fmt.Errorf("alternative %v appeared double times in preferences", alt)
		}
		m[alt] = true
	}

	for _, alt := range alts {
		_, ok := m[alt]
		if !ok { //          a better way:---->    if !m[alt] {}
			return fmt.Errorf("Can not find alternative %v in the preference", alt)
		}
	}
	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for _, pref := range prefs {
		err := checkProfile(pref, alts)
		if err != nil {
			return err
		}
	}
	return nil
}

func UniqueAlternatives(p Profile) []Alternative {
	uniqueMap := make(map[Alternative]bool)
	for _, order := range p {
		for _, alt := range order {
			if _, ok := uniqueMap[alt]; !ok {
				uniqueMap[alt] = true
			}
		}
	}
	unique := make([]Alternative, 0, len(uniqueMap))
	for alt, _ := range uniqueMap {
		unique = append(unique, alt)
	}

	return unique
}

// Condorcet1v1 returns 1 if alt1 won, 0 if tie, -1 if alt2 won
func Condorcet1v1(p Profile, alt1 Alternative, alt2 Alternative) int {

	//avoid to compare the same alternative in multiple calls by CondorcetWinner()
	if alt1 == alt2 {
		return 0
	}

	score := 0
	for _, order := range p {
		if IsPref(alt1, alt2, order) {
			score++
		} else if IsPref(alt2, alt1, order) {
			score--
		}
	}
	if score > 0 {
		return 1
	} else if score < 0 {
		return -1
	}
	return 0
}

func CondorcetWinner(p Profile) ([]Alternative, error) {
	unique := UniqueAlternatives(p)
	// check that all alternatives are in the profile
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}

	for _, alt1 := range unique {
		won := 0
		for _, alt2 := range unique {
			if Condorcet1v1(p, alt1, alt2) == 1 {
				won++
			}
		}
		if won == len(unique)-1 {
			return []Alternative{alt1}, nil
		}
	}

	return []Alternative{}, nil
}

func EliminatedSTV(p Profile, count Count) ([]Alternative, error) {
	unique := UniqueAlternatives(p)
	// check that all alternatives are in the profile
	if err := checkProfileAlternative(p, unique); err != nil {
		return nil, err
	}

	for i := range count {
		count[i] = 0
	}

	for _, votant := range p {
		for _, altPref := range votant {
			if _, ok := count[altPref]; ok {
				count[altPref] += 1
				break
			}
		}
	}

	return minCount(count), nil
}
