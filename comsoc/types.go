package comsoc

type TieBreakedSCF func(Profile) (Alternative, error)
