package comsoc

import (
	"log"
	"time"
)

type VotingServerStateInformation struct {
	VoteOpen   bool
	LastWinner Alternative
}

type VotingMessage struct {
	Preferences []Alternative
	ReplyChan   chan VotingServerStateInformation
}

type VotingServer struct {
	C          chan VotingMessage
	p          Profile
	voteOpen   bool
	serverOpen bool
	lastWinner Alternative
	Scf        func(Profile) (Alternative, error)
}

// StartVote starts a vote on the server
// it will close the vote after the specified duration
func (vs *VotingServer) StartVote(duration time.Duration) {
	vs.voteOpen = true
	log.Printf("vote open\n")
	go func(duration time.Duration) {
		time.Sleep(duration)
		vs.voteOpen = false
		log.Printf("vote closed\n")
		vs.lastWinner, _ = vs.Scf(vs.p)
	}(duration)
}

// SetSCF sets the social choice function of the server
// it only takes in "tie-broken" SCFs that always return a single alternative
func (vs *VotingServer) SetSCF(scf func(Profile) (Alternative, error)) {
	vs.Scf = scf
}

// StartServer starts listening for messages on the server channel
// is usually run in a goroutine
func (vs *VotingServer) StartServer() {
	vs.serverOpen = true
	for vs.serverOpen {
		select {
		case message := <-vs.C:
			if vs.voteOpen {
				vs.p = append(vs.p, message.Preferences)
				message.ReplyChan <- VotingServerStateInformation{vs.voteOpen, vs.lastWinner}
			} else {
				message.ReplyChan <- VotingServerStateInformation{vs.voteOpen, vs.lastWinner}
			}
		}
	}
}

func (vs *VotingServer) StopServer() {
	vs.serverOpen = false
}
