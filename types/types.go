package types

import (
	"errors"
	"sync"
	"time"

	"gitlab.utc.fr/jinxingadorni/ia04.git/comsoc"
)

type Ballot struct {
	sync.Mutex
	Id        string
	profile   comsoc.Profile
	options   [][]int
	scf       interface{}
	deadline  time.Time
	voterIds  []string
	altsCount int
	votedMap  map[string]bool
}

func (b *Ballot) AddVote(prefs []comsoc.Alternative, options []int) error {
	// TODO: validate Alternatives (check the voter does not vote for more than altsCount alternatives)
	if len(prefs) > b.altsCount {
		return errors.New("too many alternatives in the vote")
	}
	var maxAlt, minAlt comsoc.Alternative
	for _, alt := range prefs {
		if alt < minAlt {
			minAlt = alt
		}
		if alt > maxAlt {
			maxAlt = alt
		}
	}

	if minAlt < 0 || maxAlt > comsoc.Alternative(b.altsCount) {
		return errors.New("invalid alternative in the vote")
	}

	b.Lock()
	defer b.Unlock()

	b.profile = append(b.profile, prefs)
	b.options = append(b.options, options)
	return nil
}

type BallotCreationRequest struct {
	Rule      string               `json:"rule"`
	Deadline  time.Time            `json:"deadline"`
	VoterIds  []string             `json:"voter-ids"`
	AltsCount int                  `json:"#alts"`
	TieBreak  []comsoc.Alternative `json:"tie-break"`
}

type BallotCreationResponse struct {
	BallotId string `json:"ballot-id"`
}

type ResultRequest struct {
	BallotId string `json:"ballot-id"`
}

type ResultResponse struct {
	Winner comsoc.Alternative `json:"winner"`
}

type VoteRequest struct {
	BallotId string               `json:"ballot-id"`
	AgentId  string               `json:"agent-id"`
	Prefs    []comsoc.Alternative `json:"prefs"`
	Options  []int                `json:"options"`
}
